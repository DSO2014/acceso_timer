#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/gpio.h>

#include <linux/io.h>  // para mapear y desmapear direcciones de E/S en el kernel

/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/

MODULE_LICENSE("GPL");
MODULE_AUTHOR("DAC");
MODULE_DESCRIPTION("Simple module para acceder al timer de la Rpi");

/****************************************************************************/
// Globals
/****************************************************************************/

#define TIMER_BASE 0x3f003000  // pi 2 & 3, para pi1 usar 0x20003000 

#define TIMER_OFFSET 4  // lower 32 bits

static void *timer_address;
   
static int usa_timer(void)
{
	unsigned int tiempo1, tiempo2;
    
    printk(KERN_NOTICE "%s/%s(): mapping timer @ 0x%p\n",KBUILD_MODNAME,__func__, (void *)(TIMER_BASE+TIMER_OFFSET));
    timer_address = ioremap(TIMER_BASE+TIMER_OFFSET, PAGE_SIZE);
    
    if(timer_address==NULL)
    {
		printk(KERN_ERR "%s/%s(): can't map timer address\n",KBUILD_MODNAME,__func__);
		return -ENXIO;
	}

	tiempo1=ioread32(timer_address);
	
	udelay(1000);
	
	tiempo2=ioread32(timer_address);
	
	printk(KERN_NOTICE "%s/%s(): timer value... %u\n",KBUILD_MODNAME,__func__,  tiempo1);
	printk(KERN_NOTICE "%s/%s(): udelay(100)\n",KBUILD_MODNAME,__func__);
	printk(KERN_NOTICE "%s/%s(): timer value... %u\n",KBUILD_MODNAME,__func__, tiempo2);
	// tenemos en cuenta que el timer puede desbordar
	printk(KERN_NOTICE "%s/%s(): diferencia en micro segundos: %d\n",KBUILD_MODNAME,__func__, (tiempo2-tiempo1<0)? tiempo2+0xFFFF-tiempo1 : tiempo2-tiempo1);

    printk(KERN_NOTICE "%s/%s(): unmapping...\n",KBUILD_MODNAME,__func__);
	iounmap(timer_address);
    
    return 0;


}	

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/
int r_init(void) 
{	
	int res;
    printk(KERN_NOTICE "%s/%s(): Hello, loading DSO module!\n",KBUILD_MODNAME,__func__);
    
	if ((res=usa_timer())) return res;
  
    return 0;
}

void r_cleanup(void) 
{	
    printk(KERN_NOTICE "%s/%s(): DSO module cleaning up...\n",KBUILD_MODNAME,__func__);  
	printk(KERN_NOTICE "%s/%s(): Done. Bye\n",KBUILD_MODNAME,__func__);
    return;
}

module_init(r_init);
module_exit(r_cleanup);
