MODULE=timer_module

KERNEL=`uname -r`
KERNEL_SRC=/lib/modules/${KERNEL}/build
 
obj-m += ${MODULE}.o

all: uninstall compile install
 
compile:
	@echo Usando kernel ${KERNEL}
	make -C ${KERNEL_SRC} M=${CURDIR} modules

install: 
	sudo insmod ${MODULE}.ko 
	dmesg | tail 

uninstall:
	sudo rmmod ${MODULE} 
	dmesg | tail
